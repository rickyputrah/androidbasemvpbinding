package com.rickyputra.androidbasemvpbinding.provider.common;

import android.content.Context;

import com.rickyputra.androidbasemvpbinding.driver.network.APIDriver;
import com.rickyputra.androidbasemvpbinding.driver.preferences.PreferenceDriver;

/**
 * Created by ricky.harlim on 01/07/18.
 */
public class BaseProvider {
    private static Context sContext;
    private static PreferenceDriver sSharedPreferenceDriver;
    // private static DatabaseDriver sDatabaseDriver;
    private static APIDriver sAPIDriver;

    public static void init(Context context, PreferenceDriver sp, APIDriver api) {
        BaseProvider.sContext = context.getApplicationContext();
        BaseProvider.sSharedPreferenceDriver = sp;
        // BaseProvider.sDatabaseDriver = db;
        BaseProvider.sAPIDriver = api;
    }

    protected Context getContext() {
        return BaseProvider.sContext;
    }

    protected PreferenceDriver getPreferenceDriver() {
        return BaseProvider.sSharedPreferenceDriver;
    }

    public APIDriver getAPIDriver() {
        return sAPIDriver;
    }

}
