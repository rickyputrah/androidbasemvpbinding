package com.rickyputra.androidbasemvpbinding.di;

import android.app.Application;
import android.content.Context;

import com.rickyputra.androidbasemvpbinding.driver.network.RxVolleyApi;
import com.rickyputra.androidbasemvpbinding.driver.preferences.BasePreferences;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by ricky.harlim on 03/07/18.
 */

@Module
public class AppBaseModule {

    private static Application sApp;

    public AppBaseModule(Application app) {
        sApp = app;
    }

    @Provides
    @Singleton
    Context providesApplicationContext() {
        return sApp.getApplicationContext();
    }

    @Provides
    @Singleton
    Application providesApplication() {
        return sApp;
    }

    @Provides
    @Singleton
    RxVolleyApi providesAPI() {
        return new RxVolleyApi(sApp.getApplicationContext());
    }

    @Provides
    @Singleton
    BasePreferences providesPreferencDriver() {
        return new BasePreferences(sApp.getApplicationContext());
    }
}
