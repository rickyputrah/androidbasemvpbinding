package com.rickyputra.androidbasemvpbinding.di;

import android.app.Application;
import android.content.Context;

import com.rickyputra.androidbasemvpbinding.driver.network.RxVolleyApi;
import com.rickyputra.androidbasemvpbinding.driver.preferences.BasePreferences;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by ricky.harlim on 03/07/18.
 */
@Singleton
@Component(modules = {
        AppBaseModule.class
})
public interface AppBaseComponent {

    Context getApplicationContext();

    Application getApplication();

    RxVolleyApi getApi();

    BasePreferences getSharePreferences();
}
