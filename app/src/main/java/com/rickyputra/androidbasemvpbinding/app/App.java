package com.rickyputra.androidbasemvpbinding.app;

import android.app.Application;
import android.content.Context;
import android.content.res.Resources;

import com.rickyputra.androidbasemvpbinding.di.AppBaseComponent;
import com.rickyputra.androidbasemvpbinding.di.AppBaseModule;
import com.rickyputra.androidbasemvpbinding.di.DaggerAppBaseComponent;

/**
 * Created by ricky.harlim on 01/07/18.
 */
public class App extends Application {
    private static Context sContext;
    private AppBaseComponent component;

    public static App get(Context context) {
        return (App) context.getApplicationContext();
    }

    public static Resources resources() {
        return sContext.getResources();
    }

    public static Context context() {
        return sContext;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        sContext = this;
        component = DaggerAppBaseComponent.builder().appBaseModule(new AppBaseModule(this)).build();


    }

    public AppBaseComponent getComponent() {
        return component;
    }
}
