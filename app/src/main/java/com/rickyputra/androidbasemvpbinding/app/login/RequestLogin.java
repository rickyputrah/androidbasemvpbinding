package com.rickyputra.androidbasemvpbinding.app.login;

import org.parceler.Parcel;

/**
 * Created by ricky.harlim on 03/07/18.
 */
@Parcel
public class RequestLogin {
    protected String username;
    protected String password;


    public RequestLogin setUsername(String username) {
        this.username = username;
        return this;
    }


    public RequestLogin setPassword(String password) {
        this.password = password;
        return this;
    }
}
