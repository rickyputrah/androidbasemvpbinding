package com.rickyputra.androidbasemvpbinding.app.landing;

import com.rickyputra.androidbasemvpbinding._core.BasePresenter;

/**
 * Created by ricky.harlim on 05/07/18.
 */
public class LandingPresenter extends BasePresenter<LandingViewModel> {
    @Override
    protected LandingViewModel onCreateViewModel() {
        return new LandingViewModel();
    }
}
