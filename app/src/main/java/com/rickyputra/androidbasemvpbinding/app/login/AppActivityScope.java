package com.rickyputra.androidbasemvpbinding.app.login;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Scope;

/**
 * Created by ricky.harlim on 03/07/18.
 */
@Scope
@Retention(RetentionPolicy.RUNTIME)
public @interface AppActivityScope {
}
