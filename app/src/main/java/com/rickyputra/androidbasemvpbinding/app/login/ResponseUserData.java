package com.rickyputra.androidbasemvpbinding.app.login;

/**
 * Created by ricky.harlim on 04/07/18.
 */
public class ResponseUserData {


    long id;
    String name;
    String username;
    String email;
    String address;
    String phone;
    String line;
    String bbm;

    public long getId() {
        return id;
    }

    public ResponseUserData setId(long id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    public ResponseUserData setName(String name) {
        this.name = name;
        return this;
    }

    public String getUsername() {
        return username;
    }

    public ResponseUserData setUsername(String username) {
        this.username = username;
        return this;
    }

    public String getEmail() {
        return email;
    }

    public ResponseUserData setEmail(String email) {
        this.email = email;
        return this;
    }

    public String getAddress() {
        return address;
    }

    public ResponseUserData setAddress(String address) {
        this.address = address;
        return this;
    }

    public String getPhone() {
        return phone;
    }

    public ResponseUserData setPhone(String phone) {
        this.phone = phone;
        return this;
    }

    public String getLine() {
        return line;
    }

    public ResponseUserData setLine(String line) {
        this.line = line;
        return this;
    }

    public String getBbm() {
        return bbm;
    }

    public ResponseUserData setBbm(String bbm) {
        this.bbm = bbm;
        return this;
    }
}
