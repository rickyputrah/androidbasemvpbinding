package com.rickyputra.androidbasemvpbinding.app.login;

import android.databinding.ViewDataBinding;
import android.support.design.widget.Snackbar;

import com.rickyputra.androidbasemvpbinding.R;
import com.rickyputra.androidbasemvpbinding._core.BaseActivity;
import com.rickyputra.androidbasemvpbinding.app.App;
import com.rickyputra.androidbasemvpbinding.databinding.LoginActivityBinding;

public class LoginActivity extends BaseActivity<LoginPresenter, LoginViewModel> {

    private LoginActivityBinding mBinding;


    @Override
    protected ViewDataBinding onInitView(LoginViewModel viewModel) {
        mBinding = setBindView(R.layout.login_activity);
        mBinding.setViewModel(viewModel);
        mBinding.btnLogin.setOnClickListener(v -> {
            getPresenter().postLogin(mBinding.etUsername.getText().toString(), mBinding.etPassword.getText().toString());
        });
        if (getPresenter().isLogin()) {
            Snackbar a = Snackbar.make(mBinding.btnLogin, "UDAH Login", Snackbar.LENGTH_LONG);
            a.show();
        }
        return mBinding;
    }

    @Override
    public LoginPresenter createPresenter() {
        return DaggerLoginComponent.builder().appBaseComponent(App.get(this).getComponent()).loginModule(new LoginModule()).build().getPresenter();
    }

}
