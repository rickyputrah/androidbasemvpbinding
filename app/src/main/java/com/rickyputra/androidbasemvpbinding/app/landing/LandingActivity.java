package com.rickyputra.androidbasemvpbinding.app.landing;

import android.databinding.ViewDataBinding;
import android.os.Bundle;

import com.rickyputra.androidbasemvpbinding.R;
import com.rickyputra.androidbasemvpbinding._core.BaseActivity;

public class LandingActivity extends BaseActivity<LandingPresenter, LandingViewModel> {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.landing_activity);
    }

    @Override
    protected ViewDataBinding onInitView(LandingViewModel viewModel) {
        return null;
    }

    @Override
    public LandingPresenter createPresenter() {
        return null;
    }
}
