package com.rickyputra.androidbasemvpbinding.app.login;

import com.rickyputra.androidbasemvpbinding.di.AppBaseComponent;

import dagger.Component;

/**
 * Created by ricky.harlim on 03/07/18.
 */
@AppActivityScope
@Component(modules = {
        LoginModule.class
}, dependencies = AppBaseComponent.class)
public interface LoginComponent {
    LoginPresenter getPresenter();
}
