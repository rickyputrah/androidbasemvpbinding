package com.rickyputra.androidbasemvpbinding.app.login;

import android.util.Log;

import com.rickyputra.androidbasemvpbinding._core.BasePresenter;
import com.rickyputra.androidbasemvpbinding.driver.network.RxVolleyApi;
import com.rickyputra.androidbasemvpbinding.driver.preferences.BasePreferences;

import javax.inject.Inject;

import rx.Subscription;
import rx.schedulers.Schedulers;

/**
 * Created by ricky.harlim on 01/07/18.
 */
@AppActivityScope
public class LoginPresenter extends BasePresenter<LoginViewModel> {
    private RxVolleyApi mApi;
    private BasePreferences pref;

    @Inject
    LoginPresenter(RxVolleyApi api, BasePreferences pref) {
        mApi = api;
        this.pref = pref;
    }

    @Override
    protected LoginViewModel onCreateViewModel() {
        return new LoginViewModel();
    }

    public void postLogin(String username, String password) {
        RequestLogin requestLogin = new RequestLogin();
        requestLogin.setUsername(username);
        requestLogin.setPassword(password);

        Subscription request = mApi.post("http://marmarshop-clothes.com/api/login", requestLogin, RequestLogin.class, ResponseLogin.class)
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.computation())
                .subscribe(responseLogin -> handleResult(responseLogin), this::mapErrors);

        mSubscription.add(request);

    }

    private void handleResult(ResponseLogin responseLogin) {
        pref.put("User.Data", "token", responseLogin.getToken());
        Log.d("RESPONSE", responseLogin.toString());
    }

    public boolean isLogin() {
        return !pref.getString("User.Data", "token", "").isEmpty();
    }
}
