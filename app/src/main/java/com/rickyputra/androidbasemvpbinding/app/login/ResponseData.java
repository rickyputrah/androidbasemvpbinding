package com.rickyputra.androidbasemvpbinding.app.login;

/**
 * Created by ricky.harlim on 04/07/18.
 */
public class ResponseData {
    protected ResponseUserData user;

    public ResponseUserData getUser() {
        return user;
    }

    public ResponseData setUser(ResponseUserData user) {
        this.user = user;
        return this;
    }
}
