package com.rickyputra.androidbasemvpbinding.app.login;

/**
 * Created by ricky.harlim on 03/07/18.
 */
public class ResponseLogin {
    protected String token;
    protected ResponseData data;
    protected String error;


    public String getToken() {
        return token;
    }

    public ResponseLogin setToken(String token) {
        this.token = token;
        return this;
    }

    public ResponseData getData() {
        return data;
    }

    public ResponseLogin setData(ResponseData data) {
        this.data = data;
        return this;
    }

    public String getError() {
        return error;
    }

    public ResponseLogin setError(String error) {
        this.error = error;
        return this;
    }
}
