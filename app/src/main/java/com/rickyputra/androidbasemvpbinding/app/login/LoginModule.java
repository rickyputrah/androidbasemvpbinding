package com.rickyputra.androidbasemvpbinding.app.login;

import com.rickyputra.androidbasemvpbinding.driver.network.RxVolleyApi;
import com.rickyputra.androidbasemvpbinding.driver.preferences.BasePreferences;

import dagger.Module;
import dagger.Provides;

/**
 * Created by ricky.harlim on 03/07/18.
 */
@Module
public class LoginModule {

    @Provides
    LoginPresenter loginPresenter(RxVolleyApi api, BasePreferences pref) {
        return new LoginPresenter(api, pref);
    }


}
