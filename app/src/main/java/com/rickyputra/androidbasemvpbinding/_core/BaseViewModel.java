package com.rickyputra.androidbasemvpbinding._core;

import android.databinding.Bindable;

import com.rickyputra.androidbasemvpbinding.app.App;
import com.rickyputra.mvpbase.BR;
import com.rickyputra.mvpbase.base.BaseMvpViewModel;

/**
 * Created by ricky.harlim on 01/07/18.
 * <p>
 * !IMPORTANT, subclass of this view model required to use @Parcel annotation for enabling
 * automatic save and restore of view model
 * This class expands the number of attachable OnPropertyChangeCallbacks
 */

public abstract class BaseViewModel extends BaseMvpViewModel {

    protected String mToastMessage;

    public BaseViewModel() {
        super();
    }

    @Bindable
    public String getToastMessage() {
        String r = mToastMessage;
        mToastMessage = null;
        return r;
    }

    public void setToastMessage(int toastMessage) {
        mToastMessage = App.resources().getString(toastMessage);
        notifyPropertyChanged(BR.toastMessage);
    }

    public void setToastMessage(String toastMessage) {
        mToastMessage = toastMessage;
        notifyPropertyChanged(BR.toastMessage);
    }

    public boolean needToShowToast() {
        return mToastMessage != null;
    }


}
