package com.rickyputra.androidbasemvpbinding._core;

import android.app.Activity;
import android.databinding.Observable;
import android.widget.Toast;

import com.android.databinding.library.baseAdapters.BR;
import com.rickyputra.mvpbase.base.BaseMvpDialog;
import com.rickyputra.mvpbase.presenter.MvpPresenter;

/**
 * Created by ricky.harlim on 01/07/18.
 */
public abstract class BaseDialog<P extends MvpPresenter<VM>, VM extends BaseViewModel>
        extends BaseMvpDialog<P, VM> {


    public BaseDialog(Activity activity) {
        super(activity);
        setOwnerActivity(activity);
    }

    protected void onViewModelChanged(Observable observable, int i) {
        if (i == BR.toastMessage) {
            if (getViewModel().needToShowToast()) {
                Toast.makeText(getOwnerActivity(), getViewModel().getToastMessage(), Toast.LENGTH_SHORT).show();
            }
        }
    }
}
