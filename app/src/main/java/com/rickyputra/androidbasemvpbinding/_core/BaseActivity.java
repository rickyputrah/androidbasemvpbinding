package com.rickyputra.androidbasemvpbinding._core;

import android.databinding.Observable;
import android.os.Bundle;
import android.widget.Toast;

import com.f2prateek.dart.Dart;
import com.rickyputra.mvpbase.BR;
import com.rickyputra.mvpbase.base.BaseMvpActivity;
import com.rickyputra.mvpbase.presenter.MvpPresenter;

/**
 * Created by ricky.harlim on 01/07/18.
 */
public abstract class BaseActivity<P extends MvpPresenter<VM>, VM extends BaseViewModel>
        extends BaseMvpActivity<P, VM> {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Dart.inject(this);
    }

    protected void onViewModelChanged(Observable observable, int i) {
        if (i == BR.toastMessage) {
            if (getViewModel().needToShowToast()) {
                Toast.makeText(BaseActivity.this, getViewModel().getToastMessage(), Toast.LENGTH_SHORT).show();
            }
        }
    }
}

