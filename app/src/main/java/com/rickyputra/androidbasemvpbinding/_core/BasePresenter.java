package com.rickyputra.androidbasemvpbinding._core;

import android.os.Bundle;
import android.util.Log;

import com.rickyputra.mvpbase.base.BaseMvpPresenter;
import com.rickyputra.mvpbase.model.MvpViewModel;

import rx.subscriptions.CompositeSubscription;

/**
 * Created by ricky.harlim on 01/07/18.
 */
public abstract class BasePresenter<VM extends MvpViewModel> extends BaseMvpPresenter<VM> {
    public static final int REQUEST_CODE_GENERAL = 0;
    protected CompositeSubscription mSubscription;

    @Override
    protected void onCreate(Bundle presenterState) {
        super.onCreate(presenterState);
        mSubscription = new CompositeSubscription();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mSubscription.unsubscribe();
    }


    public void mapErrors(Throwable throwable) {
        mapErrors(REQUEST_CODE_GENERAL, throwable, this);
    }

    private void mapErrors(int requestCodeGeneral, Throwable throwable, BasePresenter<VM> vmBasePresenter) {
        Log.e("Error", throwable.getMessage());
    }
}
