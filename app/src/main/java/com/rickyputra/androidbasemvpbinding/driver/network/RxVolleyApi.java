package com.rickyputra.androidbasemvpbinding.driver.network;

import android.content.Context;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

import rx.Observable;

/**
 * Created by ricky.harlim on 01/07/18.
 */
public class RxVolleyApi implements APIDriver {
    private final Context mContext;

    private final RequestQueue mRequestQueue;

    public RxVolleyApi(Context context) {
        this.mContext = context;
        RequestManager.init(context);
        mRequestQueue = RequestManager.getRequestQueue();

    }

    @Override
    public <R> Observable<R> get(final String url, final Class<R> responseClass) {
        return Observable.create(subscriber -> {
            GetRequest request = new GetRequest(url
                    , response -> {
                Gson gson = new Gson();
                try {
                    R responseObject = gson.fromJson(response, responseClass);
                    subscriber.onNext(responseObject);
                } catch (Exception e) {
                    subscriber.onError(e);
                }
                subscriber.onCompleted();
            }, error -> {
                subscriber.onError(error);
            });

            addHeaders(request);
            request.setTag(responseClass);
            mRequestQueue.add(request);
        });
    }

    @Override
    public <P, R> Observable<R> post(final String url, final P body, final Class<P> requestClass, final Class<R> responseClass) {
        return Observable.create(subscriber -> {
            Gson gson = new Gson();

            PostRequest request = new PostRequest(url, gson.toJson(body)
                    , response -> {
                try {
                    Log.d("<RES>" + url, new Gson().toJson(response.toString()));
                    R responseObject = gson.fromJson(response.toString(), responseClass);

                    subscriber.onNext(responseObject);
                } catch (Exception e) {
                    subscriber.onError(e);
                }
                subscriber.onCompleted();
            }, error -> {
                subscriber.onError(error);
            });
            Log.d("<REQ>" + url, new Gson().toJson(request.mRequestBody));
            addHeaders(request);
            request.setTag(responseClass);
            mRequestQueue.add(request);
        });
    }

    private void addHeaders(Request request) {
        HashMap<String, String> params = new HashMap<String, String>();

        if (request.getMethod() == Request.Method.GET) {
            params.put("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
        } else {
            params.put("Accept", "text/json"); //NON-NLS
        }
        try {
            request.getHeaders().putAll(params);
        } catch (AuthFailureError authFailureError) {
            authFailureError.printStackTrace();
        }
    }

    public void cancelRequest() {
        mRequestQueue.cancelAll(getClass().getSimpleName());
    }


    public static class GetRequest extends StringRequest {
        Map<String, String> mHeader;

        public GetRequest(String url, Response.Listener<String> listener, Response.ErrorListener errorListener) {
            super(Request.Method.GET, url, listener, errorListener);
            this.mHeader = new HashMap<>();
        }

        @Override
        public Map<String, String> getHeaders() throws AuthFailureError {
            return mHeader;
        }
    }

    public static class PostRequest extends StringRequest {
        /**
         * Charset for request.
         */
        private static final String PROTOCOL_CHARSET = "utf-8";

        /**
         * Content type for request.
         */
        private static final String PROTOCOL_CONTENT_TYPE =
                String.format("application/json; charset=%s", PROTOCOL_CHARSET); //NON-NLS

        String mRequestBody;
        Map<String, String> mHeader;

        public PostRequest(String url, String body, Response.Listener<String> listener, Response.ErrorListener errorListener) {
            super(Method.POST, url, listener, errorListener);
            this.mRequestBody = body;
            this.mHeader = new HashMap<>();
        }

        @Override
        public Map<String, String> getHeaders() throws AuthFailureError {
            return mHeader;
        }

        @Override
        public String getBodyContentType() {
            return PROTOCOL_CONTENT_TYPE;
        }

        @Override
        public byte[] getBody() {
            try {
                return mRequestBody == null ? null : mRequestBody.getBytes(PROTOCOL_CHARSET);
            } catch (UnsupportedEncodingException uee) {
                VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", mRequestBody, PROTOCOL_CHARSET); //NON-NLS
                return null;
            }
        }
    }

}
