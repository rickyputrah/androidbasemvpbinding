package com.rickyputra.androidbasemvpbinding.driver.network;

import rx.Observable;

/**
 * Created by ricky.harlim on 01/07/18.
 */
public interface APIDriver {
    <R> Observable<R> get(String url, Class<R> responseClass);

    <P, R> Observable<R> post(final String url, final P requestBody, final Class<P> requestClass, final Class<R> responseClass);
}
