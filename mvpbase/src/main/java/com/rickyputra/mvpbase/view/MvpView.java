package com.rickyputra.mvpbase.view;

import android.content.Context;

import com.rickyputra.mvpbase.model.MvpViewModel;
import com.rickyputra.mvpbase.presenter.MvpPresenter;

/**
 * Created by ricky.harlim on 01/05/18.
 */
public interface MvpView<P extends MvpPresenter, VM extends MvpViewModel> {

    /**
     * Returns the context the view is running in, through which it can
     * access the current theme, resources, etc.
     *
     * @return The view's Context.
     */
    Context getContext();

    /**
     * Returns a current attached presenter.
     * This method is guaranteed to return a non-null value between
     * onCreate/onDetachedView and onAttachedToWindow/onDetachedFromWindow calls
     *
     * @return a currently attached presenter or null.
     */
    P getPresenter();

}
