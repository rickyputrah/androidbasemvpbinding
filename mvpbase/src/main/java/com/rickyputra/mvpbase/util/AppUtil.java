package com.rickyputra.mvpbase.util;

import android.util.Log;

/**
 * Created by ricky.harlim on 01/05/18.
 */
public class AppUtil {

    public static final boolean isVerbose = true;
    public static void log(String message){
        if(isVerbose){
            Log.i("MVPBind", message);
        }
    }

}
