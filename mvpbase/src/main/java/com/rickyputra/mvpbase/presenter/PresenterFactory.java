package com.rickyputra.mvpbase.presenter;

/**
 * Created by ricky.harlim on 01/05/18.
 */
public interface PresenterFactory<P> {
    P createPresenter();
}
