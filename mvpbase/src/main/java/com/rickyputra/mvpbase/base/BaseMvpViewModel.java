package com.rickyputra.mvpbase.base;

import android.databinding.BaseObservable;
import android.databinding.Observable;

import com.rickyputra.mvpbase.model.MvpViewModel;
import com.rickyputra.mvpbase.view.MainPropertyChangeCallback;

import org.parceler.Transient;

import java.util.HashSet;

/**
 * Created by ricky.harlim on 01/05/18.
 */

public abstract class BaseMvpViewModel extends BaseObservable implements MvpViewModel {

    @Transient
    protected OnPropertyChangedCallback mPendingPropertyChangeListener;
    @Transient
    protected HashSet<Integer> mPendings = new HashSet<>();
    @Transient
    protected boolean isPending = true;

    public BaseMvpViewModel() {
        mPendingPropertyChangeListener = new OnPropertyChangedCallback() {
            @Override
            public void onPropertyChanged(Observable observable, int i) {
                synchronized (this) {
                    if (isPending) {
                        mPendings.add(i);
                    }
                }
            }
        };
        this.addOnPropertyChangedCallback(mPendingPropertyChangeListener);
    }

    @Override
    public synchronized void addOnPropertyChangedCallback(OnPropertyChangedCallback callback) {
        super.addOnPropertyChangedCallback(callback);
        if (callback instanceof MainPropertyChangeCallback) {
            isPending = false;
        }
        synchronized (this) {
            for (Integer i : mPendings) {
                callback.onPropertyChanged(this, i);
            }
            mPendings.clear();
        }
    }

    @Override
    public synchronized void removeOnPropertyChangedCallback(OnPropertyChangedCallback callback) {
        super.removeOnPropertyChangedCallback(callback);
        if (callback instanceof MainPropertyChangeCallback) {
            isPending = true;
        }
    }
}
